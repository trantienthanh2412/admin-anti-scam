import React, { Component } from "react";
import PropTypes from "prop-types";
import { Box } from "@material-ui/core";

class SystemSetting extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  render() {
    return <Box>Quản trị hệ thống</Box>;
  }
}

export default SystemSetting;
